#!/bin/bash

function write_config {
 echo -ne $1 >> /etc/freehold/settings.json
}

function write_kv {
 write_config "\"$1\": \"$2\""
}

function next {
 write_config ',\n'
}

if [ ! -e /etc/freehold/settings.json ]; then
	echo Starting Initialization
    	cp $GOPATH/src/$HOST/tshannon/freehold/application -r /data
	ln -s $GOPATH/src/$HOST/tshannon/freehold/docs /data/docs
	write_config "{\n"
	write_kv 'address' '0.0.0.0'
	next
	write_kv 'certificateFile' ''
	next
	write_kv 'keyFile' ''
	next
	write_kv 'port' '8080'
	next
	write_kv 'datadir' '/data'
	write_config "\n}\n"
fi
$GOPATH/bin/freehold
