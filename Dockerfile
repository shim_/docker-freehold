FROM golang:1.6

ENV GOPATH /usr/src/go
ENV HOST bitbucket.org

RUN wget -O /usr/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.0.1/dumb-init_1.0.1_amd64 && \
    chmod +x /usr/bin/dumb-init && \
    mkdir /etc/freehold && \
    mkdir /data

RUN go get $HOST/tshannon/freehold && \
    go install $HOST/tshannon/freehold


COPY init.sh /usr/bin/init.sh
COPY FREEHOLD_LICENSE /FREEHOLD_LICENSE

VOLUME ["/etc/freehold","/data"]

EXPOSE 8080

WORKDIR /data

CMD ["/usr/bin/dumb-init","/usr/bin/init.sh"]
